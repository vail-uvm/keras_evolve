import argparse
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

import afpo_train


def get_parser():
    parser = argparse.ArgumentParser(
        description="Creates a summary plot from the contents of a log created by afpo_train.py.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        'log_files',
        nargs='+',
        help='One or more log files created by afpo_train.py that contains performance information to be summarized.',
    )

    return parser


def summarize_log(log_file):
    train_parser = afpo_train.get_parser()
    with open(log_file, 'r') as f:
        f.readline()
        train_args = train_parser.parse_args(f.readline().strip().split()[2:])

    df = pd.read_csv(log_file, skiprows=[0, 1, 2, 3], sep='\t')
    df = df.drop(['gen', 'gen.1', 'nevals.1', 'nevals.2', 'gen.2'], axis=1)
    df.columns = [
        'nevals', 'max_curiosity', 'mean_curiosity', 'median_curiosity',
        'min_curiosity', 'max_reward', 'mean_reward', 'median_reward', 'min_reward',
    ]
    # Note: Constrained Layout is a relatively new feature, introduced in matplotlib 3.1.
    #     As such, this may cause some issues for users that do not regularly update matplotlib.
    fig, axes = plt.subplots(3, 1, sharex='all', constrained_layout=True)

    if train_args.env_name == 'MountainCar-v0':
        axes[0].plot(np.arange(len(df.index)), -110 * np.ones(len(df.index)), '--k', label='"Solved"')
        axes[0].annotate(
            '"Solved"',
            (0.15, -110),
            xytext=(0.15, -100),
            xycoords=('axes fraction', 'data'),
            ha='center',
        )
        axes[0].set_ylim(-200, -75)

    axes[0].set_title(f'RND Performance on {train_args.env_name}')
    axes[0].set_ylabel('Max. Reward')
    axes[1].set_ylabel('Max. Curiosity')
    axes[2].set_ylabel('Episodes')

    axes[2].set_xlabel('Generation')

    df['max_reward'].plot(ax=axes[0])
    df['max_curiosity'].plot(ax=axes[1])
    # Scaled by a factor of 16 since the default behavior of afpo_train.py is to run
    # 16 environments in parallel for each policy rollout.
    data = 16 * df['nevals'].cumsum()
    data.plot(ax=axes[2])

    max_ep = data.max()
    axes[2].plot(np.arange(len(data.index)), max_ep * np.ones(len(data.index)), '--k', label='Total Episodes')
    axes[2].annotate(
        f'{int(max_ep)} Episodes',
        (0.15, max_ep),
        xytext=(0.15, max_ep + 0.1 * max_ep),
        xycoords=('axes fraction', 'data'),
        ha='center',
    )
    axes[2].set_ylim(10**2, max_ep + 0.5 * max_ep)

    plt.savefig(Path(log_file).parent / f'{Path(log_file).stem}_summary.png')


if __name__ == '__main__':
    args = get_parser().parse_args()

    sns.set()
    for log_file in args.log_files:
        summarize_log(log_file)
