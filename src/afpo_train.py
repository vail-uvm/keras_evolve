import abc
import argparse
import datetime
import functools
import logging
import multiprocessing as mp
import sys
from copy import deepcopy
from pathlib import Path

import numpy as np
from deap import algorithms
from deap import base
from deap import tools

logger = logging.getLogger(__name__)


def get_parser():
    parser = argparse.ArgumentParser(
        description="Trains a simple network on an OpenAI Gym task using Age-Fitness Pareto Optimization (AFPO).",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        '-u',
        '--updates',
        type=int,
        default=50,
        help='Number of training updates applied to the network.',
    )
    parser.add_argument(
        '-p',
        '--population_count',
        type=int,
        default=100,
        help='Number of individuals used in the evolutionary population.',
    )
    parser.add_argument(
        '-m',
        '--policy_model',
        type=str,
        default='lstm',
        choices=['dense', 'rnn', 'gru', 'lstm', 'plastic_rnn', 'plastic_gru', 'plastic_lstm', 'nm_plastic_rnn'],
        help='Primary layer used to construct the policy network.',
    )
    parser.add_argument(
        '-n',
        '--neurons',
        type=int,
        default=128,
        help='Number neurons used in hidden layers.',
    )
    parser.add_argument(
        '-e',
        '--env_name',
        type=str,
        default='Maze-v0',
        help='OpenAI Gym environment to use as the learning task.',
    )
    parser.add_argument(
        '-c',
        '--env_count',
        type=int,
        default=32,
        help='Number of environments to create using gym.vector.',
    )
    parser.add_argument(
        '--cpu_count',
        type=int,
        default=8,
        help='Number of processes to utilize for parallel evaluation.',
    )
    parser.add_argument(
        '--use_rnd',
        type=str2bool,
        default=True,
        help='Toggles the use of Random Network Distillation for intrinsic motivation.',
    )
    parser.add_argument(
        '--rnd_recurrent_input',
        type=str2bool,
        default=True,
        help='Toggles the use of the previous curiosity value as an input to the RND module.',
    )
    parser.add_argument(
        '--rnd_meta_rl_inputs',
        type=str2bool,
        default=True,
        help='Toggles use of the previous action and reward as inputs to the RND module.',
    )
    parser.add_argument(
        '--rnd_recurrent_target',
        type=str2bool,
        default=False,
        help='Toggles the use of recurrent layers for the target network of the RND module.',
    )
    parser.add_argument(
        '--rnd_recurrent_predictor',
        type=str2bool,
        default=True,
        help='Toggles the use of recurrent layers for the predictor network of the RND module.',
    )
    parser.add_argument(
        '--policy_meta_rl_inputs',
        type=str2bool,
        default=True,
        help='Toggles use of the previous action and reward as inputs to the policy network.',
    )
    parser.add_argument(
        '--combine_rewards',
        type=str2bool,
        default=False,
        help='Toggles the combination of extrinsic and intrinsic rewards if use_rnd is True.',
    )

    parser.add_argument(
        '--save_weights',
        action='store_true',
        help='Saves the weights of the models associated with the best individual.',
    )
    parser.add_argument(
        '--make_animations',
        action='store_true',
        help='Creates rollout animations using the best individual.',
    )
    parser.add_argument(
        '-v',
        '--verbose',
        action='count',
        default=0,
        help='Determines the level of terminal output.',
    )

    return parser


def str2bool(s):
    return True if s.lower() in {'true', 't', '1'} else False


# Global variables to facilitate multi-processing evaluation of individuals
agent = None
sess = None


def initializer(*args, **kwargs):
    import keras
    import tensorflow as tf
    from afpo_agent import AFPOAgent

    global sess
    sess = tf.Session()
    keras.backend.set_session(sess)

    global agent
    with sess.as_default():
        agent = AFPOAgent(*args, **kwargs)
        sess.run(tf.global_variables_initializer())


def get_weight_shapes(*args, **kwargs):
    import keras
    import tensorflow as tf
    from afpo_agent import AFPOAgent

    keras.backend.set_session(tf.Session())

    agent = AFPOAgent(*args, **kwargs)
    # Ensures that the models held by agent are properly initialized
    agent.rollout()
    return [w.shape for w in agent.get_weights()]


def evaluate(individual, episodes=2):
    with sess.as_default():
        return agent.evaluate(individual, episodes=episodes)


def make_rollout_animations(individual, output_dir):
    with sess.as_default():
        agent.set_weights(individual.weights)
        agent.rollout(make_animations=True, output_dir=output_dir)


def update_rnd_model(*args):
    with sess.as_default():
        agent.update_rnd_model()


def save_model(individual, path):
    with sess.as_default():
        agent.set_weights(individual.weights)
        agent.save_models(path)


def main(
        cpu_count=8,
        env_count=32,
        env_name='CartPole-v1',
        eval_eps=10,
        policy_model='lstm',
        neurons=32,
        population_count=100,
        updates=10,
        use_rnd=True,
        rnd_recurrent_input=True,
        rnd_meta_rl_inputs=True,
        rnd_recurrent_target=False,
        rnd_recurrent_predictor=True,
        policy_meta_rl_inputs=True,
        combine_rewards=False,
        save_weights=False,
        make_animations=False,
        output_dir=Path(),
        **kwargs,
):
    start = datetime.datetime.now()
    init_args = [
        env_name, env_count, policy_model, neurons, use_rnd, rnd_recurrent_input,
        rnd_meta_rl_inputs, rnd_recurrent_target, rnd_recurrent_predictor,
        policy_meta_rl_inputs, combine_rewards,
    ]
    with mp.Pool(processes=cpu_count, initializer=initializer, initargs=init_args) as pool:
        weight_shapes = pool.starmap(get_weight_shapes, [init_args])[0]

        toolbox = KerasAFPOToolbox(
            model_shapes=weight_shapes,
            eval_fn=evaluate,
            tournament_size=7,
            mate_indpb=0.5,
        )
        pop = toolbox.population(count=population_count)
        hof = tools.HallOfFame(15)

        reward_stats = tools.Statistics(lambda ind: ind.fitness.values[0])
        curiosity_stats = tools.Statistics(lambda ind: ind.fitness.values[1])
        stats = tools.MultiStatistics(mean_reward=reward_stats, mean_curiosity=curiosity_stats)
        stats.register("min", np.min)
        stats.register("median", np.median)
        stats.register("max", np.max)
        stats.register("mean", np.mean)

        logbook = tools.Logbook()
        logbook.header = "gen", "nevals", "mean_reward", "mean_curiosity"
        logbook.chapters["mean_reward"].header = "max", "median", "min", "mean"
        logbook.chapters["mean_curiosity"].header = "max", "median", "min", "mean"

        callbacks = []
        if use_rnd:
            update_rnd_callback = DelegateCallback(
                gen_end_func=functools.partial(pool.map, update_rnd_model, list(range(cpu_count)))
            )
            callbacks.append(update_rnd_callback)

        pop, logbook = afpo(
            pop,
            tool_box=toolbox,
            crossover_rate=0.05,
            mutation_rate=0.95,
            generations=updates,
            elitism=5,
            stats=stats,
            hall_of_fame=hof,
            logbook=logbook,
            callbacks=callbacks,
            map_fn=pool.map,
        )

        # Evaluate the best individuals more comprehensively
        final_eval = functools.partial(evaluate, episodes=eval_eps)
        fitnesses = pool.map(final_eval, hof)
        scores, _, _ = zip(*fitnesses)
        best_ind = hof[np.argmax(scores)]

        header = 'policy_model,use_rnd,rnd_recurrent_input,rnd_meta_rl_inputs,' \
                 'rnd_recurrent_target,rnd_recurrent_predictor,' \
                 'policy_meta_rl_inputs,combine_rewards'
        header_vals = f'{policy_model},{use_rnd},{rnd_recurrent_input},' \
                      f'{rnd_meta_rl_inputs},{rnd_recurrent_target},' \
                      f'{rnd_recurrent_predictor},{policy_meta_rl_inputs},' \
                      f'{combine_rewards}'

        mean_path_csv = output_dir / 'mean_reward_paths.csv'
        max_path_csv = output_dir / 'max_reward_paths.csv'
        eval_csv = output_dir / 'eval_rewards.csv'

        if not mean_path_csv.is_file():
            with open(mean_path_csv, 'w') as f:
                print(f'{header},{str(list(range(updates + 1)))[1:-1].replace(" ", "")}', file=f)

        if not max_path_csv.is_file():
            with open(max_path_csv, 'w') as f:
                print(f'{header},{str(list(range(updates + 1)))[1:-1].replace(" ", "")}', file=f)

        if not eval_csv.is_file():
            with open(eval_csv, 'w') as f:
                print(f"{header},eval_reward", file=f)

        mean_fit, max_fit = logbook.chapters['mean_reward'].select('mean', 'max')
        with open(mean_path_csv, 'a') as f:
            print(f'{header_vals},{str(mean_fit)[1:-1].replace(" ", "")}', file=f)
        with open(max_path_csv, 'a') as f:
            print(f'{header_vals},{str(max_fit)[1:-1].replace(" ", "")}', file=f)
        with open(eval_csv, 'a') as f:
            print(f'{header_vals},{max(scores)}', file=f)

        logger.info(f'Population Max Evaluation Score: {max(scores)}')

        if save_weights:
            pool.starmap(save_model, [(best_ind, output_dir)])
        if make_animations and (env_name in {'Memory-v0', 'Maze-v0'}):
            pool.starmap(
                make_rollout_animations,
                [[best_ind, output_dir]],
            )

        logger.info(f'Elapsed Time: {datetime.datetime.now() - start}')


def afpo(
        population,
        tool_box,
        crossover_rate,
        mutation_rate,
        generations,
        births=0.1,
        elitism=0,
        stats=None,
        hall_of_fame=None,
        map_fn=map,
        logbook=None,
        callbacks=None,
):
    """
    Implements Age Fitness Pareto Optimization.

    Args:
        population: (list)
        tool_box: (deap.base.Toolbox) Holds evolutionary operators.
        crossover_rate: (float)
        mutation_rate: (float)
        generations: (int)
        births: (float) Birth rate of the population as a fraction of initial population size.
        elitism: (int) Number of "best" individuals that are guaranteed to pass into the next generation unchanged.
        stats: (deap.tools.Statistics) Logs evolution dynamics.
        hall_of_fame: (deap.tools.HallOfFame) Holds best individuals.
        map_fn: (Callable)
        logbook: (deap.tools.Logbook)
        callbacks: (List[Callback])

    Returns:
        (list) Evolved population, evolution logbook.
    """
    assert births >= 0

    if callbacks is None:
        callbacks = []

    pop_count = len(population)

    if births < 1:
        births = int(len(population) * births)

    if logbook is None:
        logbook = tools.Logbook()
        logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])

    fitnesses = map_fn(tool_box.evaluate, population)
    for ind, fit in zip(population, fitnesses):
        ind.fitness.values = fit

    if hall_of_fame is not None:
        hall_of_fame.update(population)

    record = stats.compile(population) if stats else {}
    logbook.record(gen=0, nevals=len(population), **record)

    logger.info(logbook.stream)

    # The generational loop
    for gen in range(1, generations + 1):
        for callback in callbacks:
            callback.on_gen_begin()

        offspring = tool_box.select(population, pop_count - births)
        offspring = algorithms.varAnd(offspring, tool_box, crossover_rate, mutation_rate)
        offspring += [tool_box.individual() for _ in range(births)]

        if elitism:
            elites = deepcopy(hall_of_fame[:elitism])

            # Ensure that all elites are re-evaluated.
            # This should help weed out inconsistent solutions.
            for ind in elites:
                ind.fitness.delValues()
            offspring += elites

        invalids = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = map_fn(tool_box.evaluate, invalids)
        for ind, fit in zip(invalids, fitnesses):
            ind.fitness.values = fit

        if hall_of_fame is not None:
            hall_of_fame.update(offspring)

        population[:] = offspring

        record = stats.compile(population) if stats else {}
        logbook.record(gen=gen, nevals=len(invalids), **record)

        logger.info(logbook.stream)

        for individual in population:
            individual.age += 1

        for callback in callbacks:
            callback.on_gen_end()

    return population, logbook


class KerasAFPOToolbox:
    """
    A wrapper to facilitate the use of tools from DEAP to evolve the weights of
    arbitrary Keras models.
    """

    def __init__(
            self,
            model_shapes,
            eval_fn,
            tournament_size,
            mate_indpb=0.5,
            mut_mu=0.,
            map_fn=map,
    ):
        self.model_shapes = model_shapes
        self.eval_fn = eval_fn
        self.tournament_size = tournament_size
        self.mate_indpb = mate_indpb
        self.mut_mu = mut_mu
        self.map_fn = map_fn

    @staticmethod
    def clone(individual):
        return deepcopy(individual)

    def individual(self):
        return Individual(
            weights=[2 * np.random.random_sample(shape) - 1. for shape in self.model_shapes],
            fitness=AFPOFitness(),
        )

    def population(self, count):
        return [self.individual() for _ in range(count)]

    def select(self, population, count):
        """
        Args:
            population: (list) Potential tournament contestants.
            count: (int)

        Return: (list)
            Winners of each tournament.
        """
        selected = []

        for i in range(count):
            contestants = tools.selRandom(population, self.tournament_size)
            selected.append(tools.sortLogNondominated(contestants, 1, first_front_only=True)[0])
        return selected

    def mate(self, ind1, ind2):
        """
        Randomly swaps weights between two individuals.

        The mask array used to swap weights should have a boolean dtype to ensure
        that the copied components are not views of the original array.
        Additionally, this may have incorrect behavior if the individuals share
        any underlying data.

        Reference:
            http://numpy-discussion.10968.n7.nabble.com/swap-elements-in-two-arrays-td17916.html

        Args:
            ind1: (List[np.ndarray])
            ind2: (List[np.ndarray])

        Return: (tuple)
            Two individuals resulting from crossover.
        """
        swap_masks = [np.random.random(size=x.shape) < self.mate_indpb for x in ind1]
        for swap_mask, w1, w2 in zip(swap_masks, ind1, ind2):
            w1[swap_mask], w2[swap_mask] = w2[swap_mask], w1[swap_mask]

        ind1.age = max(ind1.age, ind2.age)
        ind2.age = max(ind1.age, ind2.age)

        return ind1, ind2

    def mutate(self, ind):
        """
        Args:
            ind: (List[np.ndarray])

        Returns: (List[np.ndarray])
            Mutated model weights.
        """
        # Mutate the weights of the neural network
        for w, m, s in zip(ind.weights, ind.mus, ind.sigmas):
            w += np.random.normal(loc=m, scale=s, size=w.shape)

        # Mutate the mutation strategy parameters
        for mu in ind.mus:
            np.clip(
                mu + np.random.normal(0, 1, mu.shape),
                a_min=0,
                a_max=None,
                out=mu,
            )
        for sigma in ind.sigmas:
            np.clip(
                sigma + np.random.normal(0, 1, sigma.shape),
                a_min=0,
                a_max=None,
                out=sigma,
            )

        return ind,

    def map(self, func, iterable):
        return self.map_fn(func, iterable)

    def evaluate(self, individual, **kwargs):
        return self.eval_fn(individual, **kwargs)


class Individual:
    def __init__(self, weights, fitness=None):
        self.age = 0
        self.fitness = fitness or AFPOFitness()
        self.weights = weights
        self.mus = [np.random.uniform(0, 1, w.shape) for w in self.weights]
        self.sigmas = [np.random.uniform(0, 1, w.shape) for w in self.weights]

    def __getitem__(self, item):
        return self.weights[item]

    def __setitem__(self, key, value):
        self.weights[key] = value

    def __len__(self):
        return len(self.weights)


class AFPOFitness(base.Fitness):
    # Extrinsic reward, intrinsic reward, age
    weights = (1., 1., -1.)


class Callback(abc.ABC):
    """
    A base callback class for evolutionary algorithms, providing the ability to
    execute arbitrary functions at specific points during the evolutionary cycle.

    Currently only enforces the on_gen_begin and on_gen_end methods.
    """

    @abc.abstractmethod
    def on_gen_begin(self):
        pass

    @abc.abstractmethod
    def on_gen_end(self):
        pass


class DelegateCallback(Callback):
    """
    Delegates execution to provided functions. Can be used to update global state,
    trigger functions with side effects such as logging or saving models, etc.
    """

    def __init__(self, gen_begin_func=None, gen_end_func=None):
        assert gen_begin_func or gen_end_func
        self.gen_begin_func = gen_begin_func
        self.gen_end_func = gen_end_func

    def on_gen_begin(self):
        if self.gen_begin_func is not None:
            self.gen_begin_func()

    def on_gen_end(self):
        if self.gen_end_func is not None:
            self.gen_end_func()


if __name__ == '__main__':
    args = vars(get_parser().parse_args())

    # Prevent Tensorflow warnings from contaminating logs
    logging.getLogger('tensorflow').setLevel(logging.ERROR)

    # Create an output directory to hold results
    out_path = Path(f'../results/{args["env_name"]}')
    out_path.resolve().mkdir(parents=True, exist_ok=True)

    # Configure the module logger
    formatter = logging.Formatter('%(message)s')
    handler = logging.FileHandler(str(out_path / f'{str(datetime.datetime.now()).replace(" ", "_")}.log'))
    handler.setFormatter(formatter)

    root_logger = logging.getLogger()
    root_logger.handlers = []
    root_logger.addHandler(handler)
    root_logger.setLevel(logging.INFO)

    if args['verbose']:
        std_handler = logging.StreamHandler(sys.stdout)
        std_handler.setFormatter(formatter)
        root_logger.addHandler(std_handler)
    logger.info('Command line call:\n\tpython {}'.format(' '.join(sys.argv)))

    main(**args, output_dir=out_path)
