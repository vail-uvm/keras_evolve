import argparse
import itertools
from pathlib import Path
from pprint import pprint

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats


def get_parser():
    parser = argparse.ArgumentParser(
        description="Analyzes the results from evaluating an RL algorithm on an environment.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        'file_path',
        help='Result file to be analyzed.',
    )
    parser.add_argument(
        '--reward_threshold',
        default=None,
        type=float,
        help='A reward threshold that defines what is considered to be a solution to the environment. '
             'If not specified the default behavior is to use the maximum reward.',
    )
    parser.add_argument(
        '--target_p_value',
        default=0.05,
        type=float,
        help='Target threshold for statistical significance.',
    )
    return parser


def main(file_path, reward_threshold, target_p_value):
    file_name = Path(file_path).stem

    df = pd.read_csv(file_path)
    cols = list(df.columns)
    names, chunks = zip(*df.groupby(cols[:8]))

    print(df.groupby(cols[:8]).count()['0'])

    num_conditions = len(chunks)
    t_vals = np.zeros((num_conditions, num_conditions))
    t_test_p_vals = np.zeros((num_conditions, num_conditions))
    u_vals = np.zeros((num_conditions, num_conditions))
    u_test_p_vals_g = np.zeros((num_conditions, num_conditions))
    u_test_p_vals_l = np.zeros((num_conditions, num_conditions))
    for i, j in itertools.product(range(num_conditions), range(num_conditions)):
        if reward_threshold is None:
            chunk_1 = chunks[i].iloc[:, 8:].idxmax(axis=1).astype(int)
            chunk_2 = chunks[j].iloc[:, 8:].idxmax(axis=1).astype(int)
        else:
            chunk_1 = (chunks[i].iloc[:, 8:] > reward_threshold).idxmax(axis=1).astype(int)
            chunk_2 = (chunks[j].iloc[:, 8:] > reward_threshold).idxmax(axis=1).astype(int)

        t_vals[i, j], t_test_p_vals[i, j] = stats.ttest_ind(
            chunk_1,
            chunk_2,
            equal_var=False,
        )
        u_vals[i, j], u_test_p_vals_g[i, j] = stats.mannwhitneyu(
            chunk_1,
            chunk_2,
            alternative='greater'
        )
        _, u_test_p_vals_l[i, j] = stats.mannwhitneyu(
            chunk_1,
            chunk_2,
            alternative='less'
        )

    # Bonferroni correction for multiple testing
    corrected_target_p = target_p_value / num_conditions

    # Convert double-sided p-values into single-sided p-values
    t_test_p_vals /= 2

    pd.DataFrame(t_vals).to_csv(Path(file_path).parent / f'{file_name}_t_vals.csv')
    pd.DataFrame(t_test_p_vals).to_csv(Path(file_path).parent / f'{file_name}_t_test_p_vals.csv')
    with open(Path(file_path).parent / f'{file_name}_conditions.csv', 'w') as f:
        for i, name in enumerate(names):
            print(f'{i},{name}', file=f)

    sns.heatmap(
        data=t_vals,
        annot=True,
        fmt='0.2f',
        cmap='viridis',
        vmin=-1,
        vmax=1,
    )
    plt.title('T-Test T-Values')
    plt.savefig(Path(file_path).parent / f'{file_name}_t_vals.png')
    plt.close()

    sns.heatmap(
        data=t_test_p_vals,
        annot=True,
        fmt='0.2f',
        cmap='viridis',
        vmin=0,
        vmax=1,
    )
    plt.title(f'T-Test Single Sided P-Values (Target = {target_p_value:0.4f} / {num_conditions} = {corrected_target_p:0.4f})')
    plt.savefig(Path(file_path).parent / f'{file_name}_t_test_p_vals.png')
    plt.close()

    sns.heatmap(
        data=(t_test_p_vals < target_p_value).astype(int) * np.sign(t_vals),
        annot=True,
        fmt='0.2f',
        cmap='viridis',
        vmin=-1,
        vmax=1,
    )
    plt.title(f'T-Test Single Sided P-Values < {target_p_value:0.4f}')
    plt.savefig(Path(file_path).parent / f'{file_name}_t_test_p_vals_uncorrected.png')
    plt.close()

    sns.heatmap(
        data=(t_test_p_vals < corrected_target_p).astype(int) * np.sign(t_vals),
        annot=True,
        fmt='0.2f',
        cmap='viridis',
        vmin=-1,
        vmax=1,
    )
    plt.title(f'T-Test Single Sided P-Values < {target_p_value:0.4f} / {num_conditions} = {corrected_target_p:0.4f}')
    plt.savefig(Path(file_path).parent / f'{file_name}_t_test_p_vals_corrected.png')
    plt.close()

    sns.heatmap(
        data=(u_test_p_vals_g < target_p_value).astype(int) - (u_test_p_vals_l < target_p_value).astype(int),
        annot=True,
        fmt='0.2f',
        cmap='viridis',
        vmin=-1,
        vmax=1,
    )
    plt.title(f'MWU-Test Single Sided P-Values < {target_p_value:0.4f}')
    plt.savefig(Path(file_path).parent / f'{file_name}_u_test_p_vals_uncorrected.png')
    plt.close()

    sns.heatmap(
        data=(u_test_p_vals_g < corrected_target_p).astype(int) - (u_test_p_vals_l < corrected_target_p).astype(int),
        annot=True,
        fmt='0.2f',
        cmap='viridis',
        vmin=-1,
        vmax=1,
    )
    plt.title(f'MWU-Test Single Sided P-Values < {target_p_value:0.4f} / {num_conditions} = {corrected_target_p:0.4f}')
    plt.savefig(Path(file_path).parent / f'{file_name}_u_test_p_vals_corrected.png')
    plt.close()

    if reward_threshold is None:
        reward_threshold = np.percentile(df.iloc[:, 8:].values.flatten(), 85)
    peak_df = (
        df.groupby(cols[:8])
        .apply(lambda x: (x > reward_threshold).idxmax(axis=1).astype(int))
        .reset_index(drop=True, level=-1)
    )

    index_vals = peak_df.index.unique()
    mapping = {val: ind for val, ind in zip(index_vals, range(len(index_vals)))}

    pprint(list(mapping.items()))

    peak_df = pd.concat([pd.Series(peak_df.index.map(mapping).values), peak_df.reset_index(drop=True)], axis=1)
    peak_df.columns = ['Experimental Condition', 'Generation']

    sns.boxplot(
        x='Experimental Condition',
        y='Generation',
        data=peak_df,
        palette=sns.husl_palette(n_colors=num_conditions, l=0.55),
    )
    plt.title('Distribution of Peak Reward Achievement')
    plt.savefig(Path(file_path).parent / f'{file_name}_peak_reward_boxes.png')
    plt.close()

    ts_df = df.drop(cols[:8], axis=1)
    ts_df['Experimental Condition'] = peak_df['Experimental Condition'].astype(int)
    ts_df = pd.melt(ts_df, id_vars=['Experimental Condition'])
    ts_df.rename({'variable': 'Generation', 'value': 'Reward'}, axis=1, inplace=True)
    ts_df['Generation'] = ts_df['Generation'].astype(int)

    sns.lineplot(
        x='Generation',
        y='Reward',
        hue='Experimental Condition',
        data=ts_df,
        ci=95,
        err_style='band',
        palette=sns.husl_palette(n_colors=num_conditions, l=0.55),
        linewidth=1,
    )
    plt.title('Maximum Reward Paths')
    plt.savefig(Path(file_path).parent / f'{file_name}_w_intervals.png')
    plt.close()


if __name__ == '__main__':
    main(**vars(get_parser().parse_args()))
