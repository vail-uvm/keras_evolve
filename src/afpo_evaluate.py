import argparse
import datetime
import logging
import sys
from multiprocessing import Pool
from pathlib import Path

import numpy as np
import pandas as pd

import afpo_agent

logger = logging.getLogger(__name__)


def file_check(path):
    if Path(path).exists():
        return path
    else:
        raise FileNotFoundError(f'"path"={path} is not a valid file path.')


def get_parser():
    parser = argparse.ArgumentParser(
        description="Evaluates a model trained using afpo_train.py, summarizing "
                    "the reward distribution and optionally creating rollout visualizations.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        'policy_model_path',
        type=file_check,
        help='Path to a saved policy model.',
    )
    parser.add_argument(
        '--rnd_model_path',
        type=file_check,
        help='Path to a saved RND model.',
    )
    parser.add_argument(
        '-s',
        '--eval_eps',
        type=int,
        default=3125,
        help='Number of policy rollouts to perform during evaluation.',
    )
    parser.add_argument(
        '-e',
        '--env_name',
        type=str,
        default='Maze-v0',
        help='OpenAI Gym environment to use for evaluation.',
    )
    parser.add_argument(
        '-c',
        '--env_count',
        type=int,
        default=32,
        help='Number of environments to create using gym.vector.',
    )
    parser.add_argument(
        '-g',
        '--make_gifs',
        action='store_true',
        help='Indicates whether policy rollout GIFs should be created.',
    )
    parser.add_argument(
        '-m',
        '--policy_model',
        type=str,
        default='plastic_rnn',
        choices=['dense', 'rnn', 'gru', 'lstm', 'plastic_rnn', 'plastic_gru', 'plastic_lstm'],
        help='Primary layer used to construct the agent network.',
    )
    parser.add_argument(
        '-r',
        action='store_true',
        help='Determines whether Random Network Distillation is used to promote exploration.',
    )
    parser.add_argument(
        '-p',
        '--processes',
        type=int,
        default=1,
        help='Number of processes to use for parallel evaluation.',
    )
    parser.add_argument(
        '-v',
        '--verbose',
        action='count',
        default=0,
        help='Determines the level of terminal output.',
    )

    return parser


agent = None


def initializer(env_name, env_count, use_rnd, policy_model_path, rnd_model_path):
    import keras
    import tensorflow as tf

    import plastic_layers

    # Prevent TensorFlow from allocating all available GPU memory
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    keras.backend.set_session(tf.Session(config=config))

    global agent
    agent = afpo_agent.AFPOAgent(env_name, env_count, use_rnd=use_rnd)

    custom_objects = {
        'PlasticRNN': plastic_layers.PlasticRNN,
    }

    agent.model = keras.models.load_model(
        policy_model_path,
        custom_objects=custom_objects,
    )
    if use_rnd and rnd_model_path:
        agent.rnd_model = keras.models.load_model(
            rnd_model_path,
            custom_objects=custom_objects,
        )


def rollout(*args, **kwargs):
    env_rewards, _ = agent.rollout(reduce_output=False)
    return env_rewards.flatten()


def main(
        *,
        policy_model_path,
        rnd_model_path=None,
        env_count=1,
        env_name='CartPole-v1',
        eval_eps=10,
        make_gifs=False,
        model_type='plastic_rnn',
        processes=1,
        use_rnd=True,
        **kwargs,
):
    init_args = [env_name, env_count, use_rnd, policy_model_path, rnd_model_path]
    with Pool(processes=processes, initializer=initializer, initargs=init_args) as pool:
        rewards = np.concatenate(pool.map(rollout, range(eval_eps)))

    if make_gifs:
        initializer(*init_args)
        agent.rollout(make_animations=True, output_dir=f'../out/{model_type}')

    with open(f'../out/{model_type}/eval_ep_rewards.csv', 'a') as f:
        pd.Series(rewards).to_csv(f, header=False)
    with open(f'../out/{model_type}/eval_ep_rewards_summary.csv', 'a') as f:
        pd.Series(rewards).describe().to_csv(f, header=False)


if __name__ == '__main__':
    args = vars(get_parser().parse_args())

    # Prevent Tensorflow warnings from contaminating logs
    logging.getLogger('tensorflow').setLevel(logging.ERROR)

    # Configure the module logger
    formatter = logging.Formatter('%(message)s')
    handler = logging.FileHandler(f'../out/{args["policy_model"]}/{args["policy_model"]}_{datetime.datetime.now()}.log')
    handler.setFormatter(formatter)

    root_logger = logging.getLogger()
    root_logger.handlers = []
    root_logger.addHandler(handler)
    root_logger.setLevel(logging.INFO)

    if args['verbose']:
        std_handler = logging.StreamHandler(sys.stdout)
        std_handler.setFormatter(formatter)
        root_logger.addHandler(std_handler)
    logger.info('Command line call:\n\tpython {}'.format(' '.join(sys.argv)))

    main(**args)
