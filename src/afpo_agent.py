import logging
from pathlib import Path

import gym
import gym_vail
import imageio
import numpy as np
import tensorflow as tf
from keras import backend as K, Input, Model, Sequential, layers
from keras.utils import to_categorical
from ray.rllib.env.atari_wrappers import is_atari, wrap_deepmind

import plastic_layers

gym_vail.register_envs()


def get_conv_net(output_features=64, activation='selu', final_activation='selu'):
    """
    Configuration taken from Table 4 of Rainbow, see the paper for more details:
        https://arxiv.org/pdf/1710.02298.pdf

    Args:
        output_features: keras.Tensor, Pixel inputs.
        activation:

    Returns: keras.models.Sequential
        Visual feature extractor network.
    """
    layers_ = [
        layers.Conv2D(filters=32, kernel_size=8, strides=4, activation=activation),
        layers.Conv2D(filters=64, kernel_size=4, strides=2, activation=activation),
        layers.Conv2D(filters=64, kernel_size=3, strides=1, activation=activation),
        layers.Conv2D(filters=output_features, kernel_size=1, strides=1, activation=final_activation),
        layers.GlobalAveragePooling2D(),
    ]
    return Sequential(layers_)


def get_dense_net(neurons, flatten_input=False, time_dim_output=False, activation='selu'):
    layers_ = []

    if flatten_input:
        layers_.append(layers.Flatten())

    layers_.append(layers.Dense(neurons, activation=activation))
    layers_.append(layers.Dense(neurons, activation=activation))

    if time_dim_output:
        layers_.append(layers.Reshape((1, -1)))

    return Sequential(layers_)


class AFPOAgent:
    def __init__(
            self,
            env,
            env_count,
            net_type='plastic_rnn',
            neurons=20,
            use_rnd=True,
            rnd_recurrent_input=True,
            rnd_meta_rl_inputs=True,
            rnd_recurrent_target=False,
            rnd_recurrent_predictor=True,
            policy_meta_rl_inputs=True,
            combine_rewards=False,
            feature_dim=32,
    ):
        if isinstance(env, str):
            wrappers = wrap_deepmind if is_atari(gym.make(env)) else None
            self.env = gym.vector.make(env, env_count, asynchronous=False, wrappers=wrappers)
        else:
            self.env = env
        self.env_count = env_count

        self.net_type = net_type
        self.use_rnd = use_rnd
        self.rnd_model = None
        self.neurons = neurons
        self.feature_dim = feature_dim

        self.policy_meta_rl_inputs = policy_meta_rl_inputs
        self.rnd_recurrent_input = rnd_recurrent_input
        self.rnd_meta_rl_inputs = rnd_meta_rl_inputs
        self.rnd_recurrent_target = rnd_recurrent_target
        self.rnd_recurrent_predictor = rnd_recurrent_predictor
        self.combine_rewards = combine_rewards

        self.num_actions = self.env.action_space[0].n
        self.obs_shape = self.env.observation_space.shape[1:]
        self.policy_batch_input_shape = [self.env_count, self.feature_dim + self.num_actions + 2, ]

        # CPU execution is currently faster than GPU execution.
        # This is most likely due to the high overhead of sending data to the GPU
        # combined with the large amount of CPU-GPU communication involved in a policy rollout.
        # As larger models are investigated this may change.
        with tf.device('cpu'):
            if len(self.obs_shape) < 3:
                self.feature_model = get_dense_net(neurons=self.feature_dim)
            else:
                # The CNN feature extractor seems to work on my home machine,
                # but has issues on the office machine. Must be a configuration issue?
                # Possibly software version problems?
                self.feature_model = get_conv_net(output_features=self.feature_dim)

                # Use the Dense feature extractor if this issue comes up.
                # self.feature_model = get_dense_net(neurons=self.feature_dim, flatten_input=True)

            if use_rnd:
                self.rnd_model = self.get_rnd_model()

            self.policy_model = self.get_policy_model()

        self.rnd_train_buf = UniformExperienceBuffer(4096, [self.policy_batch_input_shape[1:], ])
        self.rnd_reward_norm_buf = CircularBuffer((2048, self.env.num_envs, 1))
        self.sess = K.get_session()

    def get_policy_model(self):
        features = Input(batch_shape=self.policy_batch_input_shape)

        if self.is_recurrent_policy(self.net_type):
            pred = layers.Reshape((1, -1))(features)
        else:
            pred = features

        if self.net_type == 'plastic_rnn':
            pred = plastic_layers.PlasticRNN(self.neurons, stateful=True, return_sequences=True)(pred)
            pred = plastic_layers.PlasticRNN(self.neurons, stateful=True)(pred)
        elif self.net_type == 'plastic_gru':
            pred = plastic_layers.PlasticGRU(self.neurons, stateful=True, return_sequences=True)(pred)
            pred = plastic_layers.PlasticGRU(self.neurons, stateful=True)(pred)
        elif self.net_type == 'plastic_lstm':
            pred = plastic_layers.PlasticLSTM(self.neurons, stateful=True, return_sequences=True)(pred)
            pred = plastic_layers.PlasticLSTM(self.neurons, stateful=True)(pred)
        elif self.net_type == 'nm_plastic_rnn':
            pred = plastic_layers.NMPlasticRNN(self.neurons, stateful=True, return_sequences=True)(pred)
            pred = plastic_layers.NMPlasticRNN(self.neurons, stateful=True)(pred)
        elif self.net_type == 'rnn':
            pred = layers.SimpleRNN(self.neurons, stateful=True, return_sequences=True)(pred)
            pred = layers.SimpleRNN(self.neurons, stateful=True)(pred)
        elif self.net_type == 'gru':
            pred = layers.GRU(self.neurons, stateful=True, return_sequences=True)(pred)
            pred = layers.GRU(self.neurons, stateful=True)(pred)
        elif self.net_type == 'lstm':
            pred = layers.LSTM(self.neurons, stateful=True, return_sequences=True)(pred)
            pred = layers.LSTM(self.neurons, stateful=True)(pred)
        elif self.net_type == 'dense':
            pred = layers.Dense(units=self.neurons, activation='tanh')(pred)
            pred = layers.Dense(units=self.neurons, activation='tanh')(pred)
        else:
            raise ValueError(f'Unrecognized model_type: {self.net_type}!')

        pred_actions = layers.Dense(self.env.action_space[0].n, activation='softmax')(pred)

        model = Model(inputs=features, outputs=pred_actions)
        model.compile(optimizer='adam', loss='mse')
        return model

    @staticmethod
    def is_recurrent_policy(net_type):
        if net_type == 'dense':
            return False
        else:
            return True

    def get_rnd_model(self):
        """
        Constructs a model to perform Random Network Distillation, see the paper for details:
            https://arxiv.org/abs/1810.12894
        Returns: Keras.models.Model
            Features -> RND Curiosity
        """
        features = Input(batch_shape=self.policy_batch_input_shape)

        # Build the random target network
        if self.rnd_recurrent_target:
            random_net_1 = layers.LSTM(self.neurons, stateful=True, return_sequences=True, trainable=False)
            random_net_2 = layers.LSTM(self.neurons, stateful=True, trainable=False)
            random_pred = random_net_1(layers.Reshape((1, -1))(features))
            random_pred = random_net_2(random_pred)
        else:
            random_net_1 = layers.Dense(self.neurons, activation='tanh', trainable=False)
            random_net_2 = layers.Dense(self.neurons, activation='tanh', trainable=False)
            random_pred = random_net_1(features)
            random_pred = random_net_2(random_pred)

        # Build the prediction network
        if self.rnd_recurrent_predictor:
            predictor_1 = layers.LSTM(self.neurons, stateful=True, return_sequences=True)
            predictor_2 = layers.LSTM(self.neurons, stateful=True)
            pred = predictor_1(layers.Reshape((1, -1))(features))
            pred = predictor_2(pred)
        else:
            predictor_1 = layers.Dense(self.neurons, activation='tanh')
            predictor_2 = layers.Dense(self.neurons, activation='tanh')
            pred = predictor_1(features)
            pred = predictor_2(pred)

        # Curiosity is the MSE between their predictions
        curiosity_layer = layers.Lambda(lambda x: K.mean(K.batch_flatten((x[0] - x[1]) ** 2), axis=-1, keepdims=True))
        curiosity = curiosity_layer([pred, random_pred])

        model = Model(inputs=features, outputs=curiosity)
        # Curiosity is already a squared quantity, so we should be able to use the MAE loss
        model.compile(optimizer='adam', loss='mae')
        return model

    def update_rnd_model(self, steps=64):
        target = np.zeros((self.env_count, 1), dtype=np.float32)
        for _ in range(steps):
            input_data = self.rnd_train_buf.sample_batch(batch_size=self.env_count)
            loss = self.rnd_model.train_on_batch(input_data, target)
            logging.debug(f'RND Model Training Loss: {loss:0.4f}')

    def get_weights(self):
        return [*self.feature_model.get_weights(), *self.policy_model.get_weights()]

    def set_weights(self, weights):
        split_index = len(self.feature_model.get_weights())
        if not split_index:
            self.rollout()
            split_index = len(self.feature_model.get_weights())

        self.feature_model.set_weights(weights[:split_index])
        self.policy_model.set_weights(weights[split_index:])

    def save_models(self, output_dir='./'):
        self.feature_model.save(f'{output_dir}feature_model.h5')
        self.policy_model.save(f'{output_dir}policy_model.h5')
        if self.use_rnd:
            self.rnd_model.save(f'{output_dir}rnd_model.h5')

    def rollout(self, reduce_output=True, make_animations=False, output_dir='./'):
        frames = []
        rewards = np.zeros((self.env.num_envs, 1), dtype=np.float32)
        reward_mask = np.ones((self.env.num_envs, 1), dtype=np.float32)
        episode_rewards = np.zeros((self.env.num_envs, 1), dtype=np.float32)
        curiosity = np.zeros((self.env.num_envs, 1), dtype=np.float32)
        episode_curiosity = np.zeros((self.env.num_envs, 1), dtype=np.float32)
        actions = np.zeros((self.env.num_envs, self.num_actions), dtype=np.float32)

        states = self.env.reset()
        features = self.feature_model.predict_on_batch(states)
        while np.any(reward_mask):
            actions = self.get_actions(np.hstack([
                features,
                actions * self.policy_meta_rl_inputs,
                (rewards + curiosity * self.combine_rewards) * self.policy_meta_rl_inputs,
                curiosity * self.policy_meta_rl_inputs * (not self.combine_rewards),
            ]))
            states, rewards, dones, _ = self.env.step(actions)
            features = self.feature_model.predict_on_batch(states)
            reward_mask *= np.logical_not(dones)[..., np.newaxis]
            rewards = rewards[..., np.newaxis] * reward_mask
            actions = to_categorical(actions, num_classes=self.num_actions)

            if make_animations:
                frames.append(np.stack([e.render('rgb') for e in self.env.envs]))

            if self.use_rnd:
                rnd_obs = np.hstack([
                    features,
                    actions * self.rnd_meta_rl_inputs,
                    (rewards + curiosity * self.combine_rewards) * self.rnd_meta_rl_inputs,
                    curiosity * self.rnd_recurrent_input * (not self.combine_rewards),
                ])
                self.rnd_train_buf.insert_many([rnd_obs])
                curiosity = self.rnd_model.predict_on_batch(rnd_obs)
                self.rnd_reward_norm_buf.insert(curiosity)
                curiosity = self.rnd_reward_norm_buf.normalize(curiosity)
                logging.debug(f"Mean Intrinsic Reward: {np.mean(curiosity)}")

            episode_rewards += rewards
            episode_curiosity += curiosity
        self.policy_model.reset_states()

        if make_animations:
            # Dimensions should be (env number, time steps, features, ...)
            frames = np.stack(frames, axis=1)

            # Handle the case where there is only one feature dimension since imagio.mimwrite
            # expects 3D (frames, height, width) or 4D (frames, height, width, channels) image stacks
            if len(frames.shape) < 4:
                frames = np.expand_dims(frames, axis=-2)

            for i in range(len(frames)):
                imageio.mimwrite(Path(output_dir) / f'rollout_{i}.gif', frames[i], fps=15)

        if reduce_output:
            return np.mean(episode_rewards), np.mean(episode_curiosity)
        else:
            return episode_rewards, episode_curiosity

    def get_actions(self, obs, stochastic=False):
        action_probs = self.policy_model.predict(obs, batch_size=self.env_count)

        if stochastic:
            actions = np.array([
                np.random.choice(self.num_actions, p=probs)
                for probs in action_probs
            ])[..., np.newaxis]
        else:
            actions = np.argmax(action_probs, axis=-1)
        return actions

    def evaluate(self, individual=None, episodes=2):
        if individual:
            self.set_weights(individual.weights)

        rewards = []
        for _ in range(episodes):
            rewards.append(self.rollout())

        if individual:
            return [*np.mean(rewards, axis=0), individual.age]
        else:
            return np.mean(rewards, axis=0)


class CircularBuffer:
    def __init__(self, shape, dtype=np.float32):
        self.buffer = np.empty(shape, dtype=dtype)
        self.buffer[:] = np.nan
        self.index = 0
        self.max_size = shape[0]

    def insert(self, array):
        self.buffer[self.index] = array.copy()
        self.index = (self.index + 1) % self.max_size

    def mean(self, *args, **kwargs):
        return np.nanmean(self.buffer, *args, **kwargs)

    def std(self, *args, **kwargs):
        if np.all(np.isnan(self.buffer)) or self.index < 2:
            return 1.
        else:
            return np.nanstd(self.buffer, *args, **kwargs)

    def normalize(self, array, eps=1e-6):
        return (array - self.mean()) / (self.std() + eps)


class UniformExperienceBuffer:
    def __init__(self, max_size, input_shapes, dtype=np.float32):
        self.max_size = max_size
        self.input_shapes = input_shapes
        self.break_points = np.cumsum([np.product(x) for x in self.input_shapes])[:-1]
        self.buffer = np.empty((self.max_size, np.sum([np.product(x) for x in self.input_shapes])), dtype=dtype)
        self.buffer[:] = np.nan
        self.random_values = np.zeros(len(self.buffer)) - 1

    def insert_one(self, inputs):
        r = np.random.random()
        if np.any(r > self.random_values):
            index = self.random_values.argmin()
            self.random_values[index] = r
            self.buffer[index] = np.concatenate([x.flatten() for x in inputs])

    def insert_many(self, inputs):
        for sample in zip(*inputs):
            self.insert_one(sample)

    def sample_batch(self, batch_size=128):
        valid_inds = np.arange(len(self.random_values))[self.random_values >= 0]
        batch_inds = np.random.choice(valid_inds, batch_size)
        batch = self.buffer[batch_inds]
        batch = np.split(batch, self.break_points, axis=1)
        return [np.reshape(x, (batch_size, *s)) for x, s in zip(batch, self.input_shapes)]
