import argparse
from multiprocessing import Pool

import gym
import gym_vail
import pandas as pd

gym_vail.register_envs()


def get_parser():
    parser = argparse.ArgumentParser(
        description="Evaluates the performance of a random policy on a particular OpenAI Gym environment.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        '-c',
        '--cpu_count',
        type=int,
        default=1,
        help='Number of CPU cores available to the program.',
    )
    parser.add_argument(
        '-e',
        '--env_name',
        type=str,
        default='Maze-v0',
        help='OpenAI Gym environment to use as the learning task.',
    )
    parser.add_argument(
        '-s',
        '--num_samples',
        type=int,
        default=30,
        help='Number of episodes to run.',
    )

    parser.add_argument(
        '-v',
        '--verbose',
        action='count',
        default=0,
        help='Determines the level of terminal output.',
    )
    return parser


def random_policy_roll_out(env_name):
    env = gym.make(env_name)
    env.reset()
    done = False
    reward = 0

    while not done:
        _, step_reward, done, _ = env.step(env.action_space.sample())
        reward += step_reward

    try:
        env.close()
    except AttributeError:
        pass

    return reward


if __name__ == '__main__':
    args = vars(get_parser().parse_args())

    env_name = args['env_name']
    with Pool(processes=args['cpu_count']) as pool:
        rewards = pool.map(random_policy_roll_out, [env_name for _ in range(args['num_samples'])])

    print(pd.Series(rewards, name=f'{env_name} Random Policy Episode Rewards').describe())
