import argparse
import itertools
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats


def get_parser():
    parser = argparse.ArgumentParser(
        description="Analyzes the results from evaluating an RL algorithm on an environment.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        'file_path',
        help='Result file to be analyzed.',
    )
    parser.add_argument(
        '--target_p_value',
        default=0.05,
        type=float,
        help='Target threshold for statistical significance.',
    )
    return parser


def main(file_path, target_p_value):
    file_name = Path(file_path).stem

    df = pd.read_csv(file_path)
    cols = list(df.columns)
    names, chunks = zip(*df.groupby(cols[:-1]))
    num_conditions = len(chunks)

    print(df.groupby(cols[:-1]).count())

    # Output a performance summary (absolute)
    df_sum = df.groupby(cols[:-1]).describe()
    df_sum.to_csv(Path(file_path).parent / f'{file_name}_summary.csv')
    df_sum.to_latex(Path(file_path).parent / f'{file_name}_summary.tex')

    # Absolute performance boxplot
    score_df = df.set_index(cols[:-1])
    index_vals = score_df.index.unique()
    mapping = {val: ind for val, ind in zip(index_vals, range(len(index_vals)))}

    score_df = pd.concat([pd.Series(score_df.index.map(mapping).values), score_df.reset_index(drop=True)], axis=1)
    score_df.columns = ['Experimental Condition', 'Score']

    sns.boxplot(
        x='Experimental Condition',
        y='Score',
        data=score_df,
        palette=sns.husl_palette(n_colors=num_conditions, l=0.55),
    )
    plt.title('Distribution of Evaluation Rewards')
    plt.savefig(Path(file_path).parent / f'{file_name}_eval_reward_boxes.png')
    plt.close()

    num_conditions = len(chunks)
    t_vals = np.zeros((num_conditions, num_conditions))
    t_test_p_vals = np.zeros((num_conditions, num_conditions))
    u_vals = np.zeros((num_conditions, num_conditions))
    u_test_p_vals_g = np.zeros((num_conditions, num_conditions))
    u_test_p_vals_l = np.zeros((num_conditions, num_conditions))
    for i, j in itertools.product(range(len(chunks)), range(len(chunks))):
        t_vals[i, j], t_test_p_vals[i, j] = stats.ttest_ind(
            chunks[i]['eval_reward'],
            chunks[j]['eval_reward'],
            equal_var=False,
        )
        u_vals[i, j], u_test_p_vals_g[i, j] = stats.mannwhitneyu(
            chunks[i]['eval_reward'],
            chunks[j]['eval_reward'],
            alternative='greater'
        )
        _, u_test_p_vals_l[i, j] = stats.mannwhitneyu(
            chunks[i]['eval_reward'],
            chunks[j]['eval_reward'],
            alternative='less'
        )

    # Bonferroni correction for multiple testing
    corrected_target_p = target_p_value / num_conditions

    # Convert double-sided p-values into single-sided p-values
    t_test_p_vals /= 2

    pd.DataFrame(t_vals).to_csv(Path(file_path).parent / f'{file_name}_t_vals.csv')
    pd.DataFrame(t_test_p_vals).to_csv(Path(file_path).parent / f'{file_name}_t_test_p_vals.csv')
    with open(Path(file_path).parent / f'{file_name}_conditions.csv', 'w') as f:
        for i, name in enumerate(names):
            print(f'{i},{name}', file=f)

    sns.heatmap(
        data=t_vals,
        cmap='viridis',
        annot=True,
        fmt='0.2f',
        vmin=-1,
        vmax=1,
    )
    plt.title('T-Test T-Values')
    plt.savefig(Path(file_path).parent / f'{file_name}_t_vals.png')
    plt.close()

    sns.heatmap(
        data=t_test_p_vals,
        cmap='viridis',
        annot=True,
        fmt='0.2f',
        vmin=0,
        vmax=1,
    )
    plt.title(f'T-Test Single Sided P-Values (Target = {target_p_value:0.4f} / {num_conditions} = {corrected_target_p:0.4f})')
    plt.savefig(Path(file_path).parent / f'{file_name}_t_test_p_vals.png')
    plt.close()

    sns.heatmap(
        data=(t_test_p_vals < target_p_value).astype(int) * np.sign(t_vals),
        cmap='viridis',
        annot=True,
        fmt='0.2f',
        vmin=-1,
        vmax=1,
    )
    plt.title(f'T-Test Single Sided P-Values < {target_p_value:0.4f}')
    plt.savefig(Path(file_path).parent / f'{file_name}_t_test_p_vals_uncorrected.png')
    plt.close()

    sns.heatmap(
        data=(t_test_p_vals < corrected_target_p).astype(int) * np.sign(t_vals),
        cmap='viridis',
        annot=True,
        fmt='0.2f',
        vmin=-1,
        vmax=1,
    )
    plt.title(f'T-Test Single Sided P-Values < {target_p_value:0.4f} / {num_conditions} = {corrected_target_p:0.4f}')
    plt.savefig(Path(file_path).parent / f'{file_name}_t_test_p_vals_corrected.png')
    plt.close()

    sns.heatmap(
        data=(u_test_p_vals_g < target_p_value).astype(int) - (u_test_p_vals_l < target_p_value).astype(int),
        cmap='viridis',
        annot=True,
        fmt='0.2f',
        vmin=-1,
        vmax=1,
    )
    plt.title(f'MWU-Test Single Sided P-Values < {target_p_value:0.4f}')
    plt.savefig(Path(file_path).parent / f'{file_name}_u_test_p_vals_uncorrected.png')
    plt.close()

    sns.heatmap(
        data=(u_test_p_vals_g < corrected_target_p).astype(int) - (u_test_p_vals_l < corrected_target_p).astype(int),
        cmap='viridis',
        annot=True,
        fmt='0.2f',
        vmin=-1,
        vmax=1,
    )
    plt.title(f'MWU-Test Single Sided P-Values < {target_p_value:0.4f} / {num_conditions} = {corrected_target_p:0.4f}')
    plt.savefig(Path(file_path).parent / f'{file_name}_u_test_p_vals_corrected.png')
    plt.close()


if __name__ == '__main__':
    main(**vars(get_parser().parse_args()))
