env_name="${1:-MountainCar-v0}"
replicates="${2:-20}"

for ((i = 0 ; i < replicates ; i++)); do
    qsub -F "$env_name" experiment_1_base_agent.sh
    qsub -F "$env_name" experiment_1_combine_rewards.sh
    qsub -F "$env_name" experiment_1_no_meta_rl_rnd_input.sh
    qsub -F "$env_name" experiment_1_no_recurrent_rnd.sh
    qsub -F "$env_name" experiment_1_no_recurrent_rnd_input.sh
    qsub -F "$env_name" experiment_1_recurrent_rnd.sh
    qsub -F "$env_name" experiment_1_recurrent_rnd_target.sh
done
