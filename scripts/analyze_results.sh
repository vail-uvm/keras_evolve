#!/usr/bin/bash

folder="${1:-../results}"

for dir in "$folder"/*; do
    python ../src/analyze_eval_rewards.py "$dir"/eval_rewards.csv &
    python ../src/analyze_reward_paths.py "$dir"/max_reward_paths.csv &
done
