#PBS -l nodes=1:ppn=8,mem=32gb,vmem=64gb
#PBS -q poolmemq
#PBS -l walltime=00:10:00:00
#PBS -N experiment_1_combine_rewards
# Join STDERR TO STDOUT.  (omit this if you want separate STDOUT AND STDERR)
#PBS -j oe

env_name="${1:-MountainCar-v0}"

source activate tf_cpu
cd ~/scratch/keras_plastic/src
python afpo_train.py -m lstm -u 1000 -e "$env_name" --combine_rewards True
