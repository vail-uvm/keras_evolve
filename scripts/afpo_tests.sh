#!/usr/bin/env bash

# Timing Info:
#   2019/08/09: 9h:59m:34s

# Parallel evaluation of several neural networks applied to a simple RL task
# Each network variety is evaluated in parallel.
# Replicates for a specific network variety are evaluated sequentially.
parallel "for i in {1..30}; do python afpo_train.py -n {}; done" ::: rnn gru lstm plastic_rnn plastic_gru plastic_lstm
