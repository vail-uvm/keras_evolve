# Scripts

This directory contains bash scripts to organize and execute experiments.

Experiment 1 covers ablations that aim to investigate the effect of certain
design decisions involved in the combination of Meta-Reinforcement Learning and
Random Network Distillation for Intrinsic Motivation.

 - experiment_1_base_agent.sh:
    The default agent, all other variations are a single step change from this
    configuration. Uses separateintrinsic and extrinsic rewards, meta-RL inputs
    to the policy and RND modules, provides the previous intrinsic reward as an
    input to the RND module, non-recurrent target network in the RND module, and
    a recurrent predictor network in the RND module.
 - experiment_1_combine_rewards.sh:
    Uses combined intrinsic and extrinsic rewards for both the policy and RND
    module.
 - experiment_1_no_meta_rl_rnd_input.sh:
    The RND module only recieves the current observation as an input.
 - experiment_1_no_recurrent_rnd_input.sh:
    The RND module does not recieve the previous intrinsic reward as an input.
 - experiment_1_no_recurrent_rnd.sh:
    Both branches of the RND module are feed forward, rather than recurrent.
 - experiment_1_recurrent_rnd.sh:
    Both branches of the RND module are recurrent, rather than feed forward.
 - experiment_1_recurrent_rnd_target.sh:
    The RND module features a recurrent target branch and a feed forward
    predictor branch.
 - experiment_1.sh:
    A higher level submission script that runs multiple replicates of each of
    the experiment 1 conditions.
