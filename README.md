# keras_evolve

A framework for evolving the weights of static Keras models applied to
reinforcement learning tasks.
Primarily uses a multi-objective variant of Age Fitness Pareto Optimization
([AFPO](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.375.6168&rep=rep1&type=pdf)).


## Contents:
- results/: Contains a sub-directory of results for each task that is investigated.
- scripts/: Bash scripts for automating experiment execution on the VACC
  - `afpo_tests.sh`: Investigates the effect of neural network architecture on policy quality.
- src/
  - `afpo_agent.py`:
  - `afpo_evaluate.py`:
  - `afpo_plot_results.py`:
  - `afpo_train.py`: Optimizes the weights of a Keras model applied to an [OpenAI `gym`](https://gym.openai.com/) environment using AFPO.
  - `analyze_eval_rewards.py`:
  - `analyze_reward_paths.py`:
  - `plastic_layers.py`: Recurrent Keras layers augmented with [differentiable plasticity](https://arxiv.org/abs/1804.02464).
  - `random_policy_baseline.py`:


## Getting Started:
```bash
git clone https://gitlab.com/vail-uvm/keras_evolve.git
cd keras_evolve
pip install -r requirements.txt
```


## Notes:
 - `afpo_tests.sh` uses [GNU parallel](https://www.gnu.org/software/parallel/) to improve test runtime.
