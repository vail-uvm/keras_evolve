from setuptools import setup, find_packages


with open('requirements.txt') as f:
    requirements = [x.strip() for x in f]


setup(
    name='keras_evolve',
    version='0.0.1',
    install_requires=requirements,
    python_requires='<3.8, >3.3',
    packges=find_packages(),
)
